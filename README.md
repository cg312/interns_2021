# Reproducible Research
- [Reproducible Research Lecture Notes](reproducible/reproducible_research_lecture.md)

## Git
- [Overview](reproducible/git_overview.md)
- [Conflicts](reproducible/git_conflicts.md)
- [Team Exercise](reproducible/git_team_exercise.md)
- [Cloning and Forking](reproducible/git_cloning_and_forking.md)
